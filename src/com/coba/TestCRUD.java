package com.coba;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestCRUD {

	public static void main(String[] args) {
		selectView();
		
	}
	
	public static void selectView() {
		String sql = "SELECT * FROM VW_TASKLIST FETCH FIRST 5 ROWS ONLY";
		Connection conn = DBConnection.getConnection(); 
		Statement st = null;
		ResultSet rs = null;
		try {
			st = conn.createStatement();
			rs = st.executeQuery(sql);
			int count = 0;
			System.out.println("No.\tApp ID\t\t\t\tApp Status\t\t\tFlag Matrix\tSec Matrix");
			while (rs.next()){
				count++;
			    String appId = rs.getString("APP_ID");
			    String appStatus = rs.getString("APP_STATUS");
			    String flagMatrixProcess = rs.getString("FLAG_MATRIX_PROCESS");
			    String secFlagMatrixProcess = rs.getString("SEQ_FLAG_MATRIX_PROCESS");
			    System.out.println(count+"\t"+appId+"\t"+appStatus+"\t"+flagMatrixProcess+"\t\t"+secFlagMatrixProcess);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
			finally {
			try {
				conn.close();
				st.close();
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
	}

}

package com.coba;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class ImportCsv {
	static Connection conn;

	public static void readCsv() {
		String path = "C:\\Users\\u065637\\Documents\\New Member_Latihan\\create.csv";
		File file = new File(path);
		conn = DBConnection.getConnection();
		Statement s = null;
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			s = conn.createStatement();
			String line;
			int i = 0;
			while ((line = br.readLine()) != null) {
				try {
					String[] arr = line.split(",");
					String sql = "INSERT INTO data " + "(ref_no,nama,tgl_lahir) " + "VALUES ('" + arr[0] + "','"
							+ arr[1] + "','" + arr[2] + "') ";
					s.execute(sql);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				i++;
				System.out.println("Import success! " + i + " line");
			}
			br.close();
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public static void insertData() {
//		String path = "C:\\Users\\u065637\\Documents\\New Member_Latihan\\create.csv";
		String path = "C:\\Users\\u065637\\Documents\\New Member_Latihan\\CCOS-VELIS.csv";
		File file = new File(path);

		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		PreparedStatement ps = null;
		String line;
		int i = 0;
		try {
			while ((line = br.readLine()) != null) {
				String[] arr = line.split(",");
//				String sql = "INSERT INTO data " + "(ref_no,nama,tgl_lahir)" + "VALUES(?,?,?)";
				String sql = "INSERT INTO CCOS_VELIS " + "("
						+ "ref_no,nama,tgl_lahir,nama_ibu_kandung,"
						+ "alamat1,alamat2,alamat3,alamat4,"
						+ "no_telepon, no_hp, plavon_awal, plavon_disetujui,"
						+ "tenor, rate_pinjaman, periode_cicilan_dari, periode_cicilan_ke,"
						+ "angsuran, no_rek_realisasi, ikut_asuransi, rate_asuransi,"
						+ "premi_asuransi, provinsi, jml_pencarian_prov, jml_pencarian_prov_asuransi ) "
						+ "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
				try {
					conn = DBConnection.getConnection();
					ps = conn.prepareStatement(sql);
					ps.setString(1, arr[0]);
					ps.setString(2, arr[1]);
					ps.setString(3, arr[2]);
					ps.setString(4, arr[3]);
					ps.setString(5, arr[4]);
					ps.setString(6, arr[5]);
					ps.setString(7, arr[6]);
					ps.setString(8, arr[7]);
					ps.setString(9, arr[8]);
					ps.setString(10, arr[9]);
					ps.setString(11, arr[10]);
					ps.setString(12, arr[11]);
					ps.setString(13, arr[12]);
					ps.setString(14, arr[13]);
					ps.setString(15, arr[14]);
					ps.setString(16, arr[15]);
					ps.setString(17, arr[16]);
					ps.setString(18, arr[17]);
					ps.setString(19, arr[18]);
					ps.setString(20, arr[19]);
					ps.setString(21, arr[20]);
					ps.setString(22, arr[21]);
					ps.setString(23, arr[22]);
					ps.setString(24, arr[23]);
//					ps.setString(1, arr[0]);
//					ps.setString(2, arr[2]);
//					ps.setString(3, arr[3]);
					ps.executeQuery();
					ps.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
				i++;
				System.out.println("Import success! " + i + " line");
			}
			br.close();
			ps.close();
			conn.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}

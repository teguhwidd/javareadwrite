package com.coba;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	private static String database = "jdbc:oracle:thin:@10.20.201.184:1522:DICOS";
//	private static String database = "jdbc:oracle:thin:@10.20.201.180:1526:QICOS";
//	private static String database = "jdbc:postgresql://127.0.0.1:5432/Latihan";
	private static String user = "icos";
	private static String password = "devicos";

	public static Connection getConnection() {
		Connection conn = null;
		
		try {
			conn = DriverManager.getConnection(database, user, password);
			if (conn != null) {
				System.out.println("Connected");
			} else {
				System.out.println("Failed to make connection!");
			}
		} catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }
		return conn;
	}

}

package com.coba;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class AppendFileToZip {

	public static void main(String[] args) {
		File oldZipFile = new File("C:/coba/txt.zip");
		File[] newFiles = {new File("C:/coba/txt2.txt")};
		addFilesToZip(oldZipFile, newFiles);
	}
	public static void addFilesToZip(File oldZipFile, File[] newFiles) {
		System.out.println("START APPENDING FILES INTO ZIP");
	    try { 
	        File tmpZip = File.createTempFile(oldZipFile.getName(), null);
	        tmpZip.delete();
	        if(!oldZipFile.renameTo(tmpZip)) {
	            throw new Exception("Could not make temp file (" + oldZipFile.getName() + ")");
	        }
	        byte[] buffer = new byte[1024];
	        ZipInputStream zin = new ZipInputStream(new FileInputStream(tmpZip));
	        ZipOutputStream zout = new ZipOutputStream(new FileOutputStream(oldZipFile));
	        System.out.println("length file: "+newFiles.length);
	        for(int i = 0; i < newFiles.length; i++) {
	            InputStream in = new FileInputStream(newFiles[i]);
	            zout.putNextEntry(new ZipEntry(newFiles[i].getName()));
	            for(int read = in.read(buffer); read > -1; read = in.read(buffer)) {
	            	zout.write(buffer, 0, read);
	            }
	            zout.closeEntry();
	            in.close();
	        }

	        for(ZipEntry ze = zin.getNextEntry(); ze != null; ze = zin.getNextEntry()) {
	        	zout.putNextEntry(ze);
	            for(int read = zin.read(buffer); read > -1; read = zin.read(buffer)) {
	            	zout.write(buffer, 0, read);
	            }
	            zout.closeEntry();
	        }

	        zout.close();
	        tmpZip.delete();
	        zin.close();
	    }
	    catch(Exception e) {
	        e.printStackTrace();
	    }
	}

}
